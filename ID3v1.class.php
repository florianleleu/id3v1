<?php    
    /*
        Author : Florian Leleu
        Purpose : PHP class to read/write mp3 metadata ID3 version 1 && version 1.1
        Version : ID3v1 0.0.1
        License : CC-BY-SA, more about it in license.txt
        Contact : any ideas, critics about this? => leleu.florian@gmail.com, please write "id3v1" in the object field of your email
        Ressources (links with an * have been very useful) :    
            -http://www.id3.org/FAQ
            -http://en.wikipedia.org/wiki/ID3
            -http://fr.wikipedia.org/wiki/ID3 *
            -http://axon.cs.byu.edu/~adam/gatheredinfo/organizedtables/musicgenrelist.php
            -http://www.flashworker.de/tutorial/58/001.html *
            
            
        Last 128 bytes contain the metadata of the mp3.
        
        About ID3v1 format (128 bytes) :
            tag => "TAG" 3 bytes
            title => 30 bytes
            artist => 30 bytes
            album => 30 bytes
            year => 4 bytes
            comment => 30 bytes
            genre => 1 byte
            
        About ID3v1.1 format (128 bytes) :
            tag => "TAG" 3 bytes
            title => 30 bytes
            artist => 30 bytes
            album => 30 bytes
            year => 4 bytes
            comment => 28 bytes
            delimiter => 1 byte
            track => 1 byte
            genre => 1 byte
            
            ! WARNING: the comment part is shorter in id3v1.1 28 bytes instead of 30 but you can set the track number
                   
    */
    
    class ID3v1
    {
        //attributes
        protected $filename;
        protected $version;
        protected $title;
        protected $artist;
        protected $album;
        protected $year;
        protected $comment;
        protected $track;
        protected $genre;
        
        //constructor
        public function __construct($filename, $metadata = null, $version = "1.1")
        {
            //todo: check mime type
            if(mb_substr($filename, mb_strlen($filename) - 3) !== "mp3")
                throw new Exception($filename." is not an mp3 file");
                
            $this->filename = $filename;
            
            if($version === "1.0" || $version === "1.1")
                $this->version = $version;
            else
                throw new Exception($version. "is not a supported version of ID3, use 1.0 or 1.1");
            
            if($metadata != null && !empty($metadata) && is_array($metadata))
                $this->hydrate($metadata);
        }
        
        //setters
        public function setTitle($title)
        {
            $this->title = $title;
        }
        
        public function setArtist($artist)
        {
            $this->artist = $artist;
        }
        
        public function setAlbum($album)
        {
            $this->album = $album;
        }
        
        public function setYear($year)
        {
            if(strlen($year) != 4)
                $this->year = date('Y');
            else
                $this->year = $year;
        }
        
        public function setComment($comment)
        {
            $this->comment = $comment;
        }

        public function setTrack($track)
        {
            if($this->version === "1.1")
                $this->track = $track;
            else
                $this->track = -1;
        }
        
        public function setGenre($genre)
        {
            $this->genre = $genre;
        }
        
        //getters
        public function getVersion()
        {
            return $this->version;
        }
        
        public function getTitle()
        {
            return $this->title;
        }
        
        public function getArtist()
        {
            return $this->artist;
        }
        
        public function getAlbum()
        {
            return $this->album;
        }
        
        public function getYear()
        {
            return $this->year;
        }
        
        public function getComment()
        {
            return $this->comment;
        }
        
        public function getTrack()
        {
            return $this->track;
        }       
        
        public function getGenre()
        {
            return $this->genre;
        }
        
        //static methods
        public static function blank_fwrite($handle, $string, $length)
        {
            $offset = mb_strlen($string);
            
            if($offset < $length)
            {
                for($i = $offset ; $i < $length ; $i++)
                    $string[$i] = ' ';
            }
        
            fwrite($handle, $string, $length);
        }
        
        public static function idToGenre($id)
        {
            if(mb_strlen($id) < 2)
            {
                $id = "0".$id; 
            }
        
            $genres = @file_get_contents("genres.txt");
            if($genres)
            {
                $pattern = "/$id\ [a-z\ \/&\+]*/";
                preg_match($pattern, $genres, $match);
                if($match)
                {
                    $match = mb_substr($match[0], mb_strlen($id));
                    $match = trim($match);
                    return $match;
                }
            }
            
            return "blues";
        }
        
        public static function genreToId($genre)
        {     
            $genre = mb_strtolower($genre);
            
            $genres = @file_get_contents("genres.txt");
            if($genres)
            {
                $pattern = "/[0-9]{2,3}\ $genre\b/";
                preg_match($pattern, $genres, $match);
                if($match)
                {
                    $match = mb_substr($match[0], 0, mb_strlen($match[0]) - mb_strlen($genre));
                    $match = trim($match);
                    return $match;
                }
            }
            
            return "00";
        }
        
        //methods
        protected function hydrate($metadata)
        {
            foreach($metadata as $key => $value)
            {
                $method = 'set'.ucfirst($key);
                if(method_exists($this, $method))
                {
                    $this->$method($value);
                }
            }
        }
        
        public function read()
        {
            if(!file_exists($this->filename))
                throw new Exception($this->filename." does not exist");
                
            $mp3 = @fopen($this->filename, 'rb');
            
            if($mp3)
            {
                $pos = filesize($this->filename) - 128;
                fseek($mp3, $pos);
            
                if(fread($mp3, 3) === "TAG")
                {
                    $this->title = utf8_encode(fread($mp3, 30));
                    $this->artist = utf8_encode(fread($mp3, 30));
                    $this->album = utf8_encode(fread($mp3, 30));
                    $this->year = fread($mp3, 4);
                    
                    if($this->version === "1.0")
                    {
                        $this->comment = utf8_encode(fread($mp3, 30));
                        $this->genre = ord(fread($mp3, 1));
                    }
                    else if($this->version === "1.1")
                    {
                        $this->comment = utf8_encode(fread($mp3, 28));
                        fread($mp3, 1);
                        $this->track = ord(fread($mp3, 1));
                        $this->genre = ord(fread($mp3, 1));                    
                    }
                }
                else
                    throw new Exception($this->filename." does not contain ID3v1 informations");
                
                fclose($mp3);
            }
            else
                throw new Exception("failure while trying to open ".$this->filename);
        }    
        
        public function write()
        {
            if(!file_exists($this->filename))
                throw new Exception($this->filename." does not exist");
            
            $mp3 = @fopen($this->filename, 'rb+');
        
            if($mp3)
            {
                $pos = filesize($this->filename) - 128;
                fseek($mp3, $pos);
                
                ID3v1::blank_fwrite($mp3, "TAG", 3);          
                ID3v1::blank_fwrite($mp3, utf8_decode($this->title), 30);      
                ID3v1::blank_fwrite($mp3, utf8_decode($this->artist), 30);     
                ID3v1::blank_fwrite($mp3, utf8_decode($this->album), 30);     
                ID3v1::blank_fwrite($mp3, $this->year, 4);
                
                if($this->version === "1.0")
                {
                    ID3v1::blank_fwrite($mp3, utf8_decode($this->comment), 30);                                    
                    fwrite($mp3, chr($this->genre), 1);                      
                }
                else if($this->version === "1.1")
                {
                    ID3v1::blank_fwrite($mp3, utf8_decode($this->comment), 28);                                    
                    fwrite($mp3, chr(0), 1);
                    fwrite($mp3, chr($this->track), 1);
                    fwrite($mp3, chr($this->genre), 1);              
                }

                fclose($mp3);
            }
            else
                throw new Exception("failure while trying to open ".$this->filename);
        }
        
        public function toHTML()
        {
            echo "filename : ".$this->filename.'<br/>';
            echo "id3 version : ".$this->version.'<br/>';
            echo "title : ".$this->title.'<br/>';
            echo "artist : ".$this->artist.'<br/>';
            echo "album : ".$this->album.'<br/>';
            echo "year : ".$this->year.'<br/>';
            echo "comment : ".$this->comment.'<br/>';
            
            if($this->version === "1.1")
                echo "track : ".$this->track.'<br/>';
            
            echo "genre : ".$this->genre."(id) ".ID3v1::idToGenre($this->genre).'(genre)<br/>';
            echo "<br/>";
        }
    }
?>