<?php
    header('Content-Type: text/html; charset=utf-8');   
    require_once("ID3v1.class.php");
        
    /*
        This file is a "howto" use the id3v1 class.
        You may erase :
        mp3-examples/*
        index.php     
    */
        
        
    //the metadata you want to save in your mp3 file
    //all the data longer than the field space will be cut out
    //with id3v1 you can't set a track number and the comment size is 30 bytes
    $metadata = array
    (
        "title" => "the awesome song !",
        "artist" => "l'éléphant",
        "album" => "blue album",
        "year" => 2012,
        "comment" => "PHP un joli petit éléphant",
        "track" => 6,
        "genre" => 9
    );   
   
    
    //mp3 file, song "home" from the band "Hype" : http://www.myspace.com/hypemusic
    $filenamev1 = "mp3-examples/home-id3v1.mp3";    
    $filenamev11 = "mp3-examples/home-id3v11.mp3";    
    
    //an exception can be thrown if the file : doesn't exist/not an mp3/can't be opened/doesn't contain id3v1
    try
    {
        /* ID3 version 1.1 */
        //read the metadata 
        $mp3 = new ID3v1($filenamev11);
        $mp3->read();
        $mp3->toHTML();        
                
        //write metadata, send the array $metadata
        /*
        $mp3 = new ID3v1($filenamev11, $metadata);
        $mp3->write();        
        */

        /* ID3 version 1.0 you have to the set the version */
        //read the metadata 
        $mp3 = new ID3v1($filenamev1, null, "1.0");
        $mp3->read();
        $mp3->toHTML();        
        
        //write metadata, send the array $metadata, the field track won't be written
        /*
        $mp3 = new ID3v1($filenamev1, $metadata, "1.0");
        $mp3->write();
        */
        
        /* functions to get the genre/id, see genres.txt */
        //you can get the id from a genre, rock => 17
        echo "<br/>";
        echo ID3v1::genreToId("rock");        
        
        //or the genre from an id, 8 => jazz
        echo "<br/>";
        echo ID3v1::idToGenre(8);
    }
    catch(Exception $e)
    {
        echo $e->getMessage();
    }
?>

