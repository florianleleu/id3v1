Florian Leleu CC-BY-SA

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

-- More about it --
ID3V1 is a PHP class to read/write the metadata of mp3 files.
You can easily use it to change the metadata like the title, artist,
album, or whatever you want stored in the mp3.
Check the index.php which gives you examples of how to use the class.
You can erase the mp3-examples folder and the index.php.